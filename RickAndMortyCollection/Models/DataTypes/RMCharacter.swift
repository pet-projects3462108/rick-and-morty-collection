//
//  RMCharacter.swift
//  RickAndMortyCollection
//
//  Created by Minh Hoàng Nguyễn Viết  on 22/05/2023.
//

import Foundation

struct RMCharacter: Codable {
    let id: Int
    let name: String
    let status: RMCharacterStatus
    let species: String
    let type: String
    let gender: RMCharacterGender
    let origin: RMLocationLink
    let location: RMLocationLink
    let image: String
    let episode: [String]
    let url: String
    let created: String
}
