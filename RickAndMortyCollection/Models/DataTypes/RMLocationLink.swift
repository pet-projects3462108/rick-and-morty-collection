//
//  RMOrigin.swift
//  RickAndMortyCollection
//
//  Created by Minh Hoàng Nguyễn Viết  on 30/01/2024.
//

import Foundation

struct RMLocationLink: Codable {
    let name: String
    let url: String
}
