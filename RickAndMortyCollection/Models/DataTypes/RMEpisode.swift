//
//  RMEpisode.swift
//  RickAndMortyCollection
//
//  Created by Minh Hoàng Nguyễn Viết  on 22/05/2023.
//

import Foundation

struct RMEpisode: Codable {
    let id: Int
    let name: String
    // swiftlint:disable identifier_name
    let air_date: String
    // swiftlint:enable identifier_name
    let episode: String
    let characters: [String]
    let url: String
    let created: String
}
