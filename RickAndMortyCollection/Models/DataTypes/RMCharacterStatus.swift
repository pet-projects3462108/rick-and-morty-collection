//
//  RMCharacterStatus.swift
//  RickAndMortyCollection
//
//  Created by Minh Hoàng Nguyễn Viết  on 30/01/2024.
//

import Foundation

enum RMCharacterStatus: String, Codable {
    case alive = "Alive"
    case dead = "Dead"
    case unidentified = "unknown"

    var text: String {
        switch self {
        case .alive, .dead:
            return rawValue
        case .unidentified:
            return "Unknown"
        }
    }
}
