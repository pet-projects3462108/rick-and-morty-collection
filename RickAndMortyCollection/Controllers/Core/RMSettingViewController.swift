//
//  RMSettingViewController.swift
//  RickAndMortyCollection
//
//  Created by Minh Hoàng Nguyễn Viết  on 22/05/2023.
//

import UIKit

class RMSettingViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        title = "Settings"
    }
}
