//
//  ViewController.swift
//  RickAndMortyCollection
//
//  Created by Minh Hoàng Nguyễn Viết  on 22/05/2023.
//

import UIKit

class RMTabViewController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTabs()
    }

    private func setUpTabs() {
        let characterTabVC = UINavigationController(rootViewController: RMCharacterViewController())
        let locationTabVC = UINavigationController(rootViewController: RMLocationViewController())
        let episodeTabVC = UINavigationController(rootViewController: RMEpisodeViewController())
        let settingTabVC = UINavigationController(rootViewController: RMSettingViewController())

        characterTabVC.tabBarItem = UITabBarItem(
            title: "Character",
            image: UIImage(systemName: "person"),
            tag: 1
        )
        locationTabVC.tabBarItem = UITabBarItem(
            title: "Locations",
            image: UIImage(systemName: "globe"),
            tag: 2
        )
        episodeTabVC.tabBarItem = UITabBarItem(
            title: "Episode",
            image: UIImage(systemName: "tv"),
            tag: 3
        )
        settingTabVC.tabBarItem = UITabBarItem(
            title: "Settings",
            image: UIImage(systemName: "gear"),
            tag: 4
        )

        let tabs = [characterTabVC, locationTabVC, episodeTabVC, settingTabVC]

        for tab in tabs {
            tab.navigationBar.prefersLargeTitles = true
            tab.navigationItem.largeTitleDisplayMode = .automatic
        }

        setViewControllers(tabs, animated: true)
    }
}
