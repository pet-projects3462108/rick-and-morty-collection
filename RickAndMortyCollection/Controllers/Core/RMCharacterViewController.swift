//
//  RMCharacterViewController.swift
//  RickAndMortyCollection
//
//  Created by Minh Hoàng Nguyễn Viết  on 22/05/2023.
//

import UIKit

class RMCharacterViewController: UIViewController {
    private let listView = RMCharacterCollectionView()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Characters"
        view.backgroundColor = .systemBackground
        setupViews()
    }

    private func setupViews() {
        view.addSubview(listView)
        listView.delegate = self
        NSLayoutConstraint.activate([
            listView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            listView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            listView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
            listView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
        ])
    }
}

extension RMCharacterViewController: RMCharacterCollectionViewDelegate {
    func didSelectCharacter(
        _: RMCharacterCollectionView,
        didSelectCharacter character: RMCharacter
    ) {
        let viewModel = RMCharacterDetailViewModel(character: character)
        let detailVC = RMCharacterDetailViewController(viewModel: viewModel)
        detailVC.navigationItem.largeTitleDisplayMode = .never
        navigationController?.pushViewController(detailVC, animated: true)
    }
}
