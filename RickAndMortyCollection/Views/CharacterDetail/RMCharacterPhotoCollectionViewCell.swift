//
//  RMCharacterPhotoCollectionViewCell.swift
//  RickAndMortyCollection
//
//  Created by Minh Hoàng Nguyễn Viết  on 07/04/2024.
//

import UIKit

final class RMCharacterPhotoCollectionViewCell: UICollectionViewCell {
    static let cellIdentifier = "RMCharacterPhotoCollectionCellViewModel"

    private let image: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false

        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(image)
        addConstranints()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func addConstranints() {
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: contentView.topAnchor),
            image.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            image.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            image.leftAnchor.constraint(equalTo: contentView.leftAnchor),
        ])
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        image.image = nil
    }

    func configure(with viewModel: RMCharacterPhotoCollectionCellViewModel) {
        viewModel.fetchImage { [weak self] result in
            switch result {
            case let .success(data):
                DispatchQueue.main.async {
                    self?.image.image = UIImage(data: data)
                }
            case .failure:
                break
            }
        }
    }
}
