//
//  RMCharacterEpisodeCollectionViewCell.swift
//  RickAndMortyCollection
//
//  Created by Minh Hoàng Nguyễn Viết  on 07/04/2024.
//

import UIKit

final class RMCharacterEpisodeCollectionViewCell: UICollectionViewCell {
    static let cellIdentifier = "RMCharacterEpisodeCollectionCellViewModel"

    override init(frame: CGRect) {
        super.init(frame: frame)
        addConstranints()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func addConstranints() {}

    override func prepareForReuse() {
        super.prepareForReuse()
    }

    func configure(with _: RMCharacterEpisodeCollectionCellViewModel) {}
}
