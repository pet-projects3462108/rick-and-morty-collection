//
//  RMCharacterInfoCollectionViewCell.swift
//  RickAndMortyCollection
//
//  Created by Minh Hoàng Nguyễn Viết  on 07/04/2024.
//

import UIKit

final class RMCharacterInfoCollectionViewCell: UICollectionViewCell {
    static let cellIdentifier = "RMCharacterInfoCollectionCellViewModel"

    override init(frame: CGRect) {
        super.init(frame: frame)
        addConstranints()
        contentView.backgroundColor = .tertiarySystemBackground
        contentView.layer.cornerRadius = 8
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func addConstranints() {}

    override func prepareForReuse() {
        super.prepareForReuse()
    }

    func configure(with _: RMCharacterInfoCollectionCellViewModel) {}
}
