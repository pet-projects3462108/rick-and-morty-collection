//
//  RMLoadingMoreCollectionView.swift
//  RickAndMortyCollection
//
//  Created by Minh Hoàng Nguyễn Viết  on 27/02/2024.
//

import UIKit

class RMLoadingMoreCollectionView: UICollectionReusableView {
    static let identifier = "RMLoadingMoreCollectionView"
    private let loadingIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(style: .large)
        view.hidesWhenStopped = true
        view.translatesAutoresizingMaskIntoConstraints = false

        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .systemBackground
        addSubview(loadingIndicator)
        addConstraints()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func addConstraints() {
        NSLayoutConstraint.activate([
            loadingIndicator.heightAnchor.constraint(equalToConstant: 100),
            loadingIndicator.widthAnchor.constraint(equalToConstant: 100),
            loadingIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            loadingIndicator.centerYAnchor.constraint(equalTo: centerYAnchor),

        ])
    }

    public func startAnimating() {
        loadingIndicator.startAnimating()
    }
}
