//
//  RMRequest.swift
//  RickAndMortyCollection
//
//  Created by Minh Hoàng Nguyễn Viết  on 04/02/2024.
//

import Foundation

final class RMRequest {
    public init(
        endpoint: RMEndpoint,
        pathComponents: [String] = [],
        queryParameter: [URLQueryItem] = []
    ) {
        self.endpoint = endpoint
        self.pathComponents = pathComponents
        self.queryParameter = queryParameter
    }

    private enum Constants {
        static let baseUrl = "https://rickandmortyapi.com/api"
    }

    private let endpoint: RMEndpoint
    let pathComponents: [String]
    let queryParameter: [URLQueryItem]
    let httpRequest = "GET"

    private var urlString: String {
        var string = "\(Constants.baseUrl)/\(endpoint.rawValue)"

        for pathComponent in pathComponents {
            string += "/\(pathComponent)"
        }

        if !queryParameter.isEmpty {
            string += "?"
            let argumentString = queryParameter.compactMap {
                guard let value = $0.value else { return nil }

                return "\($0.name)=\(value)"
            }.joined(separator: "&&")
            string += argumentString
        }

        return string
    }

    public var url: URL? {
        return URL(string: urlString)
    }

    convenience init?(url: URL) {
        let string = url.absoluteString
        if !string.contains(Constants.baseUrl) {
            return nil
        }

        let trimmed = string.replacingOccurrences(of: Constants.baseUrl + "/", with: "")
        if trimmed.contains("/") {
            let components = trimmed.components(separatedBy: "/")
            if !components.isEmpty {
                var pathComponents: [String] = []
                if components.count > 1 {
                    pathComponents = components
                    pathComponents.removeFirst()
                }
                if let rmEndpoint = RMEndpoint(rawValue: components.first!) {
                    self.init(endpoint: rmEndpoint, pathComponents: pathComponents)

                    return
                }
            }
        } else if trimmed.contains("?") {
            let components = trimmed.components(separatedBy: "?")
            // Ex: character?page=2&name=t
            if components.count > 1 {
                let endpoint = components[0]
                let queryString = components[1]
                let queryItems: [URLQueryItem] = queryString.components(separatedBy: "&")
                    .compactMap {
                        guard $0.contains("=") else { return nil }

                        let part = $0.components(separatedBy: "=")

                        guard part.count == 2 else { return nil }

                        return URLQueryItem(name: part[0], value: part[1])
                    }

                if let rmEndpoint = RMEndpoint(rawValue: components.first!) {
                    self.init(endpoint: rmEndpoint, queryParameter: queryItems)

                    return
                }
            }
        }

        return nil
    }
}

extension RMRequest {
    static let listCharacterRequest = RMRequest(endpoint: .character)
}
