//
//  RMEndpoint.swift
//  RickAndMortyCollection
//
//  Created by Minh Hoàng Nguyễn Viết  on 04/02/2024.
//

import Foundation

@frozen enum RMEndpoint: String {
    case character
    case location
    case episode
}
