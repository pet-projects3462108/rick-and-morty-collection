//
//  RMCharacterEpisodeCellViewModel.swift
//  RickAndMortyCollection
//
//  Created by Minh Hoàng Nguyễn Viết  on 07/04/2024.
//

import Foundation

final class RMCharacterEpisodeCollectionCellViewModel {
    private let episodeDataUrl: URL?

    init(episodeDataUrl: URL?) {
        self.episodeDataUrl = episodeDataUrl
    }
}
