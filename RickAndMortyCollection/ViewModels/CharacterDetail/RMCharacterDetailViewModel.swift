//
//  RMCharacterDetailViewModel.swift
//  RickAndMortyCollection
//
//  Created by Minh Hoàng Nguyễn Viết  on 27/02/2024.
//

import Foundation
import UIKit

final class RMCharacterDetailViewModel {
    private let character: RMCharacter

    enum SectionType {
        case photo(viewModel: RMCharacterPhotoCollectionCellViewModel)
        case information(viewModels: [RMCharacterInfoCollectionCellViewModel])
        case episodes(viewModels: [RMCharacterEpisodeCollectionCellViewModel])
    }

    var sections: [SectionType] = []

    init(character: RMCharacter) {
        self.character = character
        setupSections()
    }

    private func setupSections() {
        sections = [
            .photo(viewModel: .init(imageUrl: URL(string: character.image))),
            .information(viewModels: [
                .init(value: character.status.text, title: "Status"),
                .init(value: character.gender.rawValue, title: "Gender"),
                .init(value: character.type, title: "Type"),
                .init(value: character.species, title: "Species"),
                .init(value: character.origin.name, title: "Origin"),
                .init(value: character.origin.name, title: "Location"),
                .init(value: character.created, title: "Created"),
                .init(value: character.episode.count.formatted(), title: "Total episode"),
            ]),
            .episodes(viewModels: character.episode
                .compactMap { .init(episodeDataUrl: URL(string: $0))
                }),
        ]
    }

    var requestUrl: URL? {
        return URL(string: character.url)
    }

    var title: String { character.name.uppercased() }

    func createPhotoSection() -> NSCollectionLayoutSection {
        let item = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .fractionalHeight(1.0)
        ))
        item.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 10, trailing: 0)
        let group = NSCollectionLayoutGroup.vertical(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .fractionalHeight(0.5)
            ),
            subitems: [item]
        )
        let section = NSCollectionLayoutSection(group: group)

        return section
    }

    func createInformationSection() -> NSCollectionLayoutSection {
        let item = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(0.5),
            heightDimension: .fractionalHeight(1.0)
        ))
        item.contentInsets = NSDirectionalEdgeInsets(top: 2, leading: 2, bottom: 2, trailing: 2)
        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .absolute(150)
            ),
            subitems: [item, item]
        )
        let section = NSCollectionLayoutSection(group: group)

        return section
    }

    func createEpisodesSection() -> NSCollectionLayoutSection {
        let item = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .fractionalHeight(1.0)
        ))
        item.contentInsets = NSDirectionalEdgeInsets(top: 10, leading: 5, bottom: 10, trailing: 8)
        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(0.8),
                heightDimension: .absolute(150)
            ),
            subitems: [item]
        )
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .groupPaging

        return section
    }
}
