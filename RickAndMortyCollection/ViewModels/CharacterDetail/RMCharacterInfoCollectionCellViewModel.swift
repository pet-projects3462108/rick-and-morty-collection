//
//  RMCharacterInfoCollectionCellViewModel.swift
//  RickAndMortyCollection
//
//  Created by Minh Hoàng Nguyễn Viết  on 07/04/2024.
//

import Foundation

final class RMCharacterInfoCollectionCellViewModel {
    let value: String
    let title: String

    init(value: String, title: String) {
        self.value = value
        self.title = title
    }
}
