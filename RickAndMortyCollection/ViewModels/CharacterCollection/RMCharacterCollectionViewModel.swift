//
//  RMCharacterListViewModel.swift
//  RickAndMortyCollection
//
//  Created by Minh Hoàng Nguyễn Viết  on 07/02/2024.
//

import Foundation
import UIKit

protocol RMCharacterCollectionViewModelDelegate: AnyObject {
    func didLoadInitialCharacters()

    func didLoadMoreCharacters(with newIndexPaths: [IndexPath])

    func didSelectCharacter(_ character: RMCharacter)
}

final class RMCharacterCollectionViewModel: NSObject {
    weak var delegate: RMCharacterCollectionViewModelDelegate?
    private var characters: [RMCharacter] = [] {
        didSet {
            cellViewModels = characters.map { RMCharacterCollectionCellViewModel(
                characterName: $0.name,
                characterStatus: $0.status,
                characterImageUrl: URL(string: $0.image)
            )
            }
        }
    }

    private var cellViewModels: [RMCharacterCollectionCellViewModel] = []
    private var pageInfo: RMGetAllCharacterResponse.Info?
    private var canLoadMore: Bool { pageInfo?.next != nil }
    private var isLoadingMore = false

    func fetch() {
        RMService.shared
            .execute(.listCharacterRequest,
                     expecting: RMGetAllCharacterResponse.self)
        { [weak self] data in
            switch data {
            case let .success(response):
                self?.characters = response.results
                self?.pageInfo = response.info
                DispatchQueue.main.async {
                    self?.delegate?.didLoadInitialCharacters()
                }
            case let .failure(error):
                print(String(describing: error))
            }
        }
    }

    func loadMore(url: URL) {
        guard !isLoadingMore else { return }
        isLoadingMore = true
        guard let request = RMRequest(url: url) else { return }

        RMService.shared
            .execute(request, expecting: RMGetAllCharacterResponse.self) { [weak self] data in
                guard let self else { return }
                switch data {
                case let .success(response):
                    let currenCharacterCount = self.characters.count
                    let newCharacters = response.results
                    self.characters.append(contentsOf: newCharacters)
                    self.pageInfo = response.info
                    let newIndexPaths =
                        Array(currenCharacterCount ..<
                            (currenCharacterCount + newCharacters.count))
                        .compactMap { IndexPath(row: $0, section: 0) }
                    DispatchQueue.main.async {
                        self.delegate?.didLoadMoreCharacters(with: newIndexPaths)
                        self.isLoadingMore = false
                    }
                case let .failure(error):
                    print(String(describing: error))
                    self.isLoadingMore = false
                }
            }
    }
}

extension RMCharacterCollectionViewModel: UICollectionViewDataSource, UICollectionViewDelegate,
    UICollectionViewDelegateFlowLayout
{
    func collectionView(_: UICollectionView, numberOfItemsInSection _: Int) -> Int {
        return cellViewModels.count
    }

    func collectionView(_ collectonView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        guard let cell = collectonView.dequeueReusableCell(
            withReuseIdentifier: RMCharacterCollectionViewCell.cellIdentifier,
            for: indexPath
        ) as? RMCharacterCollectionViewCell else {
            fatalError("Unsupported cell")
        }

        cell.configure(with: cellViewModels[indexPath.row])

        return cell
    }

    func collectionView(
        _: UICollectionView,
        layout _: UICollectionViewLayout,
        sizeForItemAt _: IndexPath
    ) -> CGSize {
        let bounds = UIScreen.main.bounds
        let width = (bounds.width - 30) / 2

        return CGSize(width: width, height: width * 1.5)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        delegate?.didSelectCharacter(characters[indexPath.row])
    }

    func collectionView(
        _ collectionView: UICollectionView,
        viewForSupplementaryElementOfKind kind: String,
        at indexPath: IndexPath
    ) -> UICollectionReusableView {
        guard kind == UICollectionView
            .elementKindSectionFooter, let footer = collectionView.dequeueReusableSupplementaryView(
                ofKind: kind,
                withReuseIdentifier: RMLoadingMoreCollectionView.identifier,
                for: indexPath
            ) as? RMLoadingMoreCollectionView else { fatalError("Unsupported") }

        footer.startAnimating()

        return footer
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout _: UICollectionViewLayout,
        referenceSizeForFooterInSection _: Int
    ) -> CGSize {
        guard canLoadMore else { return .zero }
        return CGSize(width: collectionView.frame.width, height: 100)
    }
}

extension RMCharacterCollectionViewModel: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard canLoadMore,
              !isLoadingMore,
              !cellViewModels.isEmpty,
              let nextUrl = URL(string: (pageInfo?.next)!) else { return }

        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { [weak self] in
            let offset = scrollView.contentOffset.y
            let totalScrollHeight = scrollView.contentSize.height
            let totalScrollFrameHeight = scrollView.frame.size.height

            if offset >= totalScrollHeight - totalScrollFrameHeight - 120 {
                self?.loadMore(url: nextUrl)
            }
            $0.invalidate()
        }
    }
}
